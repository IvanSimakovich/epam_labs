﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FirstWebApplication.ViewModels
{
    public class RegisterViewModel
    {

            [Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }

            [Required]
            [Display(Name = "Login")]
            public string Login { get; set; }
        
            [Display(Name = "Home address")]
            public string Adress { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }


            [Required]
            [Compare("Password", ErrorMessage = "Password confirm fail.")]
            [DataType(DataType.Password)]
            [Display(Name = "Confirm Password")]
            public string PasswordConfirm { get; set; }

            [Display(Name = "Email")]
            public string Email { get; set; }

            [Display(Name = "FirstName")]
            public string FirstName { get; set; }

            [Display(Name = "LastName")]
            public string LastName { get; set; }
        
            [Display(Name = "Localization")]
            public string Localization { get; set; }


    }
}
