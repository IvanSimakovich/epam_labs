﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FirstWebApplication.ViewModels
{
    public class LoginViewModel
    {

        //[Required]
        //[Display(Name = "Login")]
        //public string Login { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Login")]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        

        [Display(Name = "RememberMe?")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}