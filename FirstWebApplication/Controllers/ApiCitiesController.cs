using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FirstWebApplication.Data;
using FirstWebApplication.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace FirstWebApplication.Controllers
{
    [Produces("application/json")]
    [Route("api/ApiCities")]
    public class ApiCitiesController : Controller
    {
        private readonly FirstWebApplicationContext _context;

        public ApiCitiesController(FirstWebApplicationContext context)
        {
            _context = context;
        }

        // GET: api/ApiCities
        [HttpGet]
        public JArray /*IEnumerable<City>*/ GetCitys()
        {
            var citys = _context.Citys
                     .Include(u => u.Venues)
                     .ToList();            

            return JArray.FromObject(citys, new JsonSerializer()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }

        // GET: api/ApiCities/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCity([FromRoute] int? id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var city = await _context.Citys.SingleOrDefaultAsync(m => m.ID == id);

            if (city == null)
            {
                return NotFound();
            }

            return Ok(city);
        }

        // PUT: api/ApiCities/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCity([FromRoute] int? id, [FromBody] City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != city.ID)
            {
                return BadRequest();
            }

            _context.Entry(city).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiCities
        [HttpPost]
        public async Task<IActionResult> PostCity([FromBody] City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Citys.Add(city);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCity", new { id = city.ID }, city);
        }

        // DELETE: api/ApiCities/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCity([FromRoute] int? id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var city = await _context.Citys.SingleOrDefaultAsync(m => m.ID == id);
            if (city == null)
            {
                return NotFound();
            }

            _context.Citys.Remove(city);
            await _context.SaveChangesAsync();

            return Ok(city);
        }

        private bool CityExists(int? id)
        {
            return _context.Citys.Any(e => e.ID == id);
        }
    }
}