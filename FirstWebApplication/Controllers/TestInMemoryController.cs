using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FirstWebApplication.Models;
using FirstWebApplication.Data;

namespace FirstWebApplication.Controllers
{
    public class TestInMemoryController : Controller
    {
            private readonly ApiMemoryContext _context;

            public TestInMemoryController(ApiMemoryContext context)
            {
                _context = context;
            }

            public ActionResult Get()
            {
                var citys = _context.Citys
                    .Include(u => u.Venues)
                    .ToList();

                //var response = citys.Select(u => new
                //{
                //    Name = u.Name,
                //    VenueName = u.Venues.Select(p => p.Name),
                //    VenueAdress = u.Venues.Select(p=>p.Address)
                //});

                return View(citys);
            }
     }
 }