﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApplication.Models
{
    public enum StatusTicket { Selling, Waitingforconfirmation, Confirmed, Rejected, Sold };
    public class Ticket
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int? ID { get; set; }

        public int? EventID { get; set; }
        public User Seller { get; set; }
        public decimal Price { get; set; }

        public User User { get; set; }
        public Event Event { get; set; }
    }
}

