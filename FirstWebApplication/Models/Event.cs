﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FirstWebApplication.Models
{
    public class Event
    {

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int? ID { get; set; }

        public string Name { get; set; }
        public int? VenueID { get; set; }
        public DateTime Date { get; set; }
        public string BannerUrl { get; set; }
        public string Description { get; set; }
        
        public Venue Venue { get; set; }
    }
}
