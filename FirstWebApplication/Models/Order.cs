﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace FirstWebApplication.Models
{
    public enum StatusOrder { WaitingForConfirmation, Confirmed, Rejected };
    public class Order
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int? ID { get; set; }


        public int? TicketID { get; set; }
        public User Buyer { get; set; }
        public string TrackNo { get; set; }

        public StatusOrder Status { get; set; }


        public User User { get; set; }
        public Ticket Ticket { get; set; }
    }
}
