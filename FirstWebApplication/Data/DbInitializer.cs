﻿using System;
using System.Linq;
using FirstWebApplication.Data;
using FirstWebApplication.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace FirstWebApplication.Data
{
    public static class DbInitializer
    {
        public static void Initialize(FirstWebApplicationContext firstWebContext)
        {
            firstWebContext.Database.EnsureCreated();

            if (!firstWebContext.Users.Any())
            {
                var users = new List<User>()
                {
                        new User{FirstName = "Ivan", LastName="Simakovich", Adress="Gomel, Privokzalnay 1", Localization="ru-Ru", Email="user@gmail.com", Login="user1", Password="user"},
                        new User{FirstName = "Alexey", LastName="Harshkalep", Adress="Gomel, Privokzalnay 2", Localization="en-En", Email="admin@gmail.com", Login="admin1", Password="admin"},
                        new User{FirstName = "User", LastName="User", Adress="Gomel, Privokzalnay 3", Localization="en-En", Email="user@gmail.com", Login="user2", Password="user"}
                };
                firstWebContext.Users.AddRange(users);
                firstWebContext.SaveChanges();
            }
            
            firstWebContext.Database.EnsureCreated();
            
            if (!firstWebContext.Citys.Any())
            {
                var cities = new List<City>()
                {
                    new City
                    {
                        Name = "Minsk"
                    },
                    new City
                    {
                        Name = "New York"
                    },
                    new City
                    {
                        Name = "Paris"
                    },
                    new City
                    {
                        Name = "Berlin"
                    },
                    new City
                    {
                        Name = "Moskov"
                    },
                    new City
                    {
                        Name = "Gomel"
                    }
                };
                firstWebContext.Citys.AddRange(cities);
                firstWebContext.SaveChanges();
            }

            if (!firstWebContext.Venues.Any())
            {
                List<Venue> venues = new List<Venue>()
                {
                    new Venue{CityID = 1, Name = "Minsk Arena", Address = "Nezavisimosti 12"},
                    new Venue{CityID = 1, Name = "Centralnui dom oficerov",Address = "Dorofeeva 76"},
                    new Venue{CityID = 2, Name = "Club-Star",  Address = "Independence 43"},
                    new Venue{CityID = 2, Name = "Blackhall bar", Address = "Hellsway 23"},
                    new Venue{CityID = 3, Name = "Mulen Ruge",Address = "Elisey 1"},
                    new Venue{CityID = 3, Name = "Bastilia", Address = "Monmatr 57"},
                    new Venue{CityID = 4, Name = "DK Veteranov", Address = "Abricosovay 30"},
                    new Venue{CityID = 4, Name = "TNT CLUB", Address = "Kremenchugskay 54"},
                    new Venue{CityID = 5, Name = "Prime-hall", Address = "Grinbur 28"},
                    new Venue{CityID = 5, Name = "Kronshtadt", Address = "Lonstal 67"},
                    new Venue{CityID = 6, Name = "GCK", Address = "Irininskay 18"},
                    new Venue{CityID = 6, Name = "Dvorec  Jeleznodorojnikov", Address = "Privokzalnay 1"},
                };

                firstWebContext.Venues.AddRange(venues);
                firstWebContext.SaveChanges();
            }

            if (!firstWebContext.Events.Any())
            {
                var events = new List<Event>()
                {
                    new Event{Name = "Justin Timberlake", VenueID = 1 , Date = new DateTime(2017, 11, 16),
                        BannerUrl = @"http://poster.by/db/b0005614.jpg", Description = "Posledniy concert, tolko segodny" },
                    new Event{Name = "The Offspring", VenueID = 2 , Date = new DateTime(2017, 09, 03),
                         BannerUrl = @"http://poster.by/db/b0007611.jpg", Description =  "Posledniy concert, tolko segodny" },
                    new Event{Name = "Kristina Agilera", VenueID = 3 , Date = new DateTime(2017, 09, 07),
                         BannerUrl = @"http://poster.by/db/b0005379.jpg", Description =  "Description field 3" },
                    new Event{Name = "Avril Lavin", VenueID = 4 , Date = new DateTime(2017, 10, 03),
                         BannerUrl = @"http://poster.by/db/b0007074.jpg", Description =  "Description field 4" },
                    new Event{Name = "Shakira", VenueID = 5 , Date = new DateTime(2017, 10, 15),
                         BannerUrl = @"http://poster.by/db/b0007985.jpg", Description =  "Description field 5" },
                    new Event{Name = "Muse", VenueID = 6 , Date = new DateTime(2017, 11, 25),
                         BannerUrl = @"http://poster.by/db/b0008215.jpg", Description =  "Description field 6" },
                    new Event{Name = "Roling Stones", VenueID = 7 , Date = new DateTime(2017, 12, 04),
                         BannerUrl = @"http://poster.by/db/b0006156.jpg", Description =  "Description field 7" },
                    new Event{Name = "ACDC", VenueID = 8 , Date = new DateTime(2017, 10, 27),
                         BannerUrl = @"http://poster.by/db/b0028670.jpg", Description =  "Description field 8" },
                    new Event{Name = "Lady Gaga", VenueID = 9 , Date = new DateTime(2017, 09, 26),
                         BannerUrl = @"http://poster.by/db/b0017793.jpg", Description =  "Description field 9" },
                    new Event{Name = "Gorilaz", VenueID = 10 , Date = new DateTime(2017, 10, 26),
                         BannerUrl = @"http://poster.by/db/b0006139.jpg", Description =  "Description field 10" },
                    new Event{Name = "Scorpions", VenueID = 11 , Date = new DateTime(2017, 11, 07),
                         BannerUrl = @"http://poster.by/db/b0016013.jpg", Description =  "Description field 11" },
                    new Event{Name = "Metallica", VenueID = 12 , Date = new DateTime(2017, 09, 01),
                         BannerUrl = @"http://poster.by/db/b0008195.jpg", Description =  "Description field 12" },
                };

                firstWebContext.Events.AddRange(events);
                firstWebContext.SaveChanges();
            }

            if (!firstWebContext.Tickets.Any())
            {
                var ticets = new List<Ticket>()
                {
                    new Ticket{EventID = 1, Price=111, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 1) },
                    new Ticket{EventID = 2, Price=112, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 1) },
                    new Ticket{EventID = 3, Price=113, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 1) },
                    new Ticket{EventID = 4, Price=121, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 1) },
                    new Ticket{EventID = 5, Price=122, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 2) },
                    new Ticket{EventID = 6, Price=123, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 2) },
                    new Ticket{EventID = 7, Price=131, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 2) },
                    new Ticket{EventID = 8, Price=132, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 2) },
                    new Ticket{EventID = 9, Price=133, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 3) },
                    new Ticket{EventID = 10, Price=211, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 3) },
                    new Ticket{EventID = 11, Price=212, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 3) },
                    new Ticket{EventID = 12, Price=222, Seller=firstWebContext.Users.FirstOrDefault(u => u.Id == 3) }
                };

                firstWebContext.Tickets.AddRange(ticets);
                firstWebContext.SaveChanges();
            }
            
            if (!firstWebContext.Orders.Any())
            {
                var orders = new List<Order>()
                {
                    new Order{ TicketID = 1, TrackNo="asdqw241", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 1) , Status =StatusOrder.WaitingForConfirmation},
                    new Order{ TicketID = 2, TrackNo="ghsdhfd4", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 2) , Status=StatusOrder.WaitingForConfirmation},
                    new Order{ TicketID = 3, TrackNo="asdqdgsdgfw241", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 3) , Status=StatusOrder.WaitingForConfirmation},
                    new Order{ TicketID = 4, TrackNo="ssbxcert", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 1) , Status=StatusOrder.Confirmed},
                    new Order{ TicketID = 5, TrackNo="iklsdfger34", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 2) , Status=StatusOrder.Confirmed},
                    new Order{ TicketID = 6, TrackNo="asdf34xcvbm", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 3) , Status=StatusOrder.Confirmed},
                    new Order{ TicketID = 7, TrackNo="547dbhddf2", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 1) , Status=StatusOrder.Confirmed},
                    new Order{ TicketID = 8, TrackNo="sdgf245645", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 2) , Status=StatusOrder.Confirmed},
                    new Order{ TicketID = 9, TrackNo="sdfg2345", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 3) , Status=StatusOrder.Confirmed},
                    new Order{ TicketID = 10, TrackNo="astr2", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 1) , Status=StatusOrder.Rejected},
                    new Order{ TicketID = 11, TrackNo="jtyjko54623", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 2) , Status=StatusOrder.Rejected},
                    new Order{ TicketID = 12, TrackNo="fdaaaaaaaaa", Buyer = firstWebContext.Users.FirstOrDefault(u => u.Id == 3) , Status=StatusOrder.Rejected},
                };

                firstWebContext.Orders.AddRange(orders);
                firstWebContext.SaveChanges();
            }

            firstWebContext.SaveChanges();
        }
    }
}
