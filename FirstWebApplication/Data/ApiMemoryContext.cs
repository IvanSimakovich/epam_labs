﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using FirstWebApplication.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApplication.Data
{
    public class ApiMemoryContext:DbContext
    {
            public ApiMemoryContext(DbContextOptions<ApiMemoryContext> options)
                : base(options)
            {
            }        

            public DbSet<City> Citys { get; set; }

            public DbSet<Venue> Venues { get; set; }

            public DbSet<User> Users { get; set; }

            public DbSet<Event> Events { get; set; }

            public DbSet<Ticket> Tickets { get; set; }



            //protected override void OnModelCreating(ModelBuilder modelBuilder)
            //{
            //    base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<City>().ToTable("Citys");

            //modelBuilder.Entity<Venue>().ToTable("Venues");
            //}
    }

}
