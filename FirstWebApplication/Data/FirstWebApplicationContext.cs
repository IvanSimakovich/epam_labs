﻿using FirstWebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace FirstWebApplication.Data
{
    public class FirstWebApplicationContext : DbContext
    {
        public FirstWebApplicationContext(DbContextOptions<FirstWebApplicationContext> options) : base(options)
        {
        }

        public DbSet<City> Citys { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<User> Users { get; set; }

        public DbSet<FirstWebApplication.ViewModels.RegisterViewModel> RegisterViewModel { get; set; }
        public DbSet<FirstWebApplication.ViewModels.LoginViewModel> LoginViewModel { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().ToTable("City");
            modelBuilder.Entity<Venue>().ToTable("Venue");
            modelBuilder.Entity<Event>().ToTable("Event");
            modelBuilder.Entity<Ticket>().ToTable("Ticket");
            modelBuilder.Entity<Order>().ToTable("Order");
            modelBuilder.Entity<User>().ToTable("User");
        }
    }
}