﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstWebApplication.Models;

namespace FirstWebApplication.Data
{
    public class AddTestData
    {
        public static void AddNewTestData(ApiMemoryContext context)
        {
            if (context.Citys.Any())
            {
                return;
            }
            else
            {
                List<City> cities = new List<City>()
                {
                    new City
                    {
                        Name = "Minsk"
                    },
                    new City
                    {
                        Name = "New York"
                    },
                    new City
                    {
                        Name = "Paris"
                    },
                    new City
                    {
                        Name = "Berlin"
                    },
                    new City
                    {
                        Name = "Moskov"
                    },
                    new City
                    {
                        Name = "Gomel"
                    }
                };
                    context.Citys.AddRange(cities);
            }

            if (context.Venues.Any())
            {
                return;
            }
            else
            {
                List<Venue> venues = new List<Venue>()
                {
                    new Venue{CityID = 1, Name = "Minsk Arena", Address = "Nezavisimosti 12"},
                    new Venue{CityID = 1, Name = "Centralnui dom oficerov",Address = "Dorofeeva 76"},
                    new Venue{CityID = 2, Name = "Club-Star",  Address = "Independence 43"},
                    new Venue{CityID = 2, Name = "Blackhall bar", Address = "Hellsway 23"},
                    new Venue{CityID = 3, Name = "Mulen Ruge",Address = "Elisey 1"},
                    new Venue{CityID = 3, Name = "Bastilia", Address = "Monmatr 57"},
                    new Venue{CityID = 4, Name = "DK Veteranov", Address = "Abricosovay 30"},
                    new Venue{CityID = 4, Name = "TNT CLUB", Address = "Kremenchugskay 54"},
                    new Venue{CityID = 5, Name = "Prime-hall", Address = "Grinbur 28"},
                    new Venue{CityID = 5, Name = "Kronshtadt", Address = "Lonstal 67"},
                    new Venue{CityID = 6, Name = "GCK", Address = "Irininskay 18"},
                    new Venue{CityID = 6, Name = "Dvorec  Jeleznodorojnikov", Address = "Privokzalnay 1"},
                };
                context.Venues.AddRange(venues);
            }
            context.SaveChanges();
        }
    }
}
