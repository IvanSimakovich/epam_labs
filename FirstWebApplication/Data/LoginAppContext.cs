﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using FirstWebApplication.Models;
using FirstWebApplication.ViewModels;


namespace FirstWebApplication.Data
{
    public class LoginAppContext : DbContext
    {
        public LoginAppContext(DbContextOptions<LoginAppContext> options)
        : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<FirstWebApplication.ViewModels.RegisterViewModel> RegisterViewModel { get; set; }
        public DbSet<FirstWebApplication.ViewModels.LoginViewModel> LoginViewModel { get; set; }


    }
}
