﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using FirstWebApplication.Data;
using FirstWebApplication.Models;

namespace FirstWebApplication.Migrations
{
    [DbContext(typeof(FirstWebApplicationContext))]
    partial class FirstWebApplicationContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("FirstWebApplication.Models.City", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("City");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Event", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BannerUrl");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<int?>("VenueID");

                    b.HasKey("ID");

                    b.HasIndex("VenueID");

                    b.ToTable("Event");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Order", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BuyerLogin");

                    b.Property<int>("Status");

                    b.Property<int?>("TicketID");

                    b.Property<string>("TrackNo");

                    b.Property<string>("UserLogin");

                    b.HasKey("ID");

                    b.HasIndex("BuyerLogin");

                    b.HasIndex("TicketID");

                    b.HasIndex("UserLogin");

                    b.ToTable("Order");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Ticket", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("EventID");

                    b.Property<decimal>("Price");

                    b.Property<string>("SellerLogin");

                    b.Property<string>("UserLogin");

                    b.HasKey("ID");

                    b.HasIndex("EventID");

                    b.HasIndex("SellerLogin");

                    b.HasIndex("UserLogin");

                    b.ToTable("Ticket");
                });

            modelBuilder.Entity("FirstWebApplication.Models.User", b =>
                {
                    b.Property<string>("Login")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Adress");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<int>("Id");

                    b.Property<string>("LastName");

                    b.Property<string>("Localization");

                    b.Property<string>("Password");

                    b.HasKey("Login");

                    b.ToTable("User");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Venue", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int?>("CityID");

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.HasIndex("CityID");

                    b.ToTable("Venue");
                });

            modelBuilder.Entity("FirstWebApplication.ViewModels.LoginViewModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Login")
                        .IsRequired();

                    b.Property<string>("Password")
                        .IsRequired();

                    b.Property<bool>("RememberMe");

                    b.Property<string>("ReturnUrl");

                    b.HasKey("Id");

                    b.ToTable("LoginViewModel");
                });

            modelBuilder.Entity("FirstWebApplication.ViewModels.RegisterViewModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Adress");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Localization");

                    b.Property<string>("Login")
                        .IsRequired();

                    b.Property<string>("Password")
                        .IsRequired();

                    b.Property<string>("PasswordConfirm")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("RegisterViewModel");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Event", b =>
                {
                    b.HasOne("FirstWebApplication.Models.Venue", "Venue")
                        .WithMany()
                        .HasForeignKey("VenueID");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Order", b =>
                {
                    b.HasOne("FirstWebApplication.Models.User", "Buyer")
                        .WithMany()
                        .HasForeignKey("BuyerLogin");

                    b.HasOne("FirstWebApplication.Models.Ticket", "Ticket")
                        .WithMany()
                        .HasForeignKey("TicketID");

                    b.HasOne("FirstWebApplication.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserLogin");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Ticket", b =>
                {
                    b.HasOne("FirstWebApplication.Models.Event", "Event")
                        .WithMany()
                        .HasForeignKey("EventID");

                    b.HasOne("FirstWebApplication.Models.User", "Seller")
                        .WithMany()
                        .HasForeignKey("SellerLogin");

                    b.HasOne("FirstWebApplication.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserLogin");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Venue", b =>
                {
                    b.HasOne("FirstWebApplication.Models.City", "City")
                        .WithMany("Venues")
                        .HasForeignKey("CityID");
                });
        }
    }
}
