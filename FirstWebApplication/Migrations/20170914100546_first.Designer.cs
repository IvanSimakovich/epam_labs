﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using FirstWebApplication.Data;
using FirstWebApplication.Models;

namespace FirstWebApplication.Migrations
{
    [DbContext(typeof(FirstWebApplicationContext))]
    [Migration("20170914100546_first")]
    partial class first
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("FirstWebApplication.Models.City", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("City");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Event", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BannerUrl");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<int?>("VenueID");

                    b.HasKey("ID");

                    b.HasIndex("VenueID");

                    b.ToTable("Event");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Order", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("BuyerId");

                    b.Property<int>("Status");

                    b.Property<int?>("TicketID");

                    b.Property<string>("TrackNo");

                    b.HasKey("ID");

                    b.HasIndex("BuyerId");

                    b.HasIndex("TicketID");

                    b.ToTable("Order");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Ticket", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("EventID");

                    b.Property<decimal>("Price");

                    b.Property<int?>("SellerIDId");

                    b.HasKey("ID");

                    b.HasIndex("EventID");

                    b.HasIndex("SellerIDId");

                    b.ToTable("Ticket");
                });

            modelBuilder.Entity("FirstWebApplication.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Adress");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Localization");

                    b.Property<string>("Login");

                    b.Property<string>("Password");

                    b.HasKey("Id");

                    b.ToTable("User");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Venue", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int?>("CityID");

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.HasIndex("CityID");

                    b.ToTable("Venue");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Event", b =>
                {
                    b.HasOne("FirstWebApplication.Models.Venue", "Venue")
                        .WithMany()
                        .HasForeignKey("VenueID");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Order", b =>
                {
                    b.HasOne("FirstWebApplication.Models.User", "Buyer")
                        .WithMany()
                        .HasForeignKey("BuyerId");

                    b.HasOne("FirstWebApplication.Models.Ticket", "Ticket")
                        .WithMany()
                        .HasForeignKey("TicketID");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Ticket", b =>
                {
                    b.HasOne("FirstWebApplication.Models.Event", "Feast")
                        .WithMany()
                        .HasForeignKey("EventID");

                    b.HasOne("FirstWebApplication.Models.User", "SellerID")
                        .WithMany()
                        .HasForeignKey("SellerIDId");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Venue", b =>
                {
                    b.HasOne("FirstWebApplication.Models.City", "City")
                        .WithMany("Venues")
                        .HasForeignKey("CityID");
                });
        }
    }
}
