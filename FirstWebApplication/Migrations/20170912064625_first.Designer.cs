﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using FirstWebApplication.Data;

namespace FirstWebApplication.Migrations
{
    [DbContext(typeof(ApiMemoryContext))]
    [Migration("20170912064625_first")]
    partial class first
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("FirstWebApplication.Models.City", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("Citys");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Event", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BannerUrl");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<int?>("VenueID");

                    b.HasKey("ID");

                    b.HasIndex("VenueID");

                    b.ToTable("Events");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Ticket", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("FeastID");

                    b.Property<decimal>("Price");

                    b.Property<int?>("SellerId");

                    b.HasKey("Id");

                    b.HasIndex("FeastID");

                    b.HasIndex("SellerId");

                    b.ToTable("Tickets");
                });

            modelBuilder.Entity("FirstWebApplication.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Adress");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Localization");

                    b.Property<string>("Password");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Venue", b =>
                {
                    b.Property<int?>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int?>("CityID");

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.HasIndex("CityID");

                    b.ToTable("Venues");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Event", b =>
                {
                    b.HasOne("FirstWebApplication.Models.Venue", "Venue")
                        .WithMany()
                        .HasForeignKey("VenueID");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Ticket", b =>
                {
                    b.HasOne("FirstWebApplication.Models.Event", "Feast")
                        .WithMany()
                        .HasForeignKey("FeastID");

                    b.HasOne("FirstWebApplication.Models.User", "Seller")
                        .WithMany()
                        .HasForeignKey("SellerId");
                });

            modelBuilder.Entity("FirstWebApplication.Models.Venue", b =>
                {
                    b.HasOne("FirstWebApplication.Models.City", "City")
                        .WithMany("Venues")
                        .HasForeignKey("CityID");
                });
        }
    }
}
